# 介绍

短剧管理系统是一个综合性的后台管理平台，旨在为短剧内容的创建、发布、管理和监控提供一站式解决方案。该系统支持从内容分类到用户反馈的全方位管理，确保短剧制作团队和平台运营者能够高效、有序地工作。

## 管理端

- 仪表盘

  仪表盘作为系统的核心，提供了实时数据监控和关键性能指标的可视化展示，帮助管理者快速把握平台的运营状况。

  ![仪表盘](doc/images/管理端/仪表盘.png)

- 分类管理
  通过分类管理，管理员可以创建和维护短剧的分类体系，使用户能够根据兴趣快速找到相应的内容。

  ![分类管理](doc/images/管理端/分类管理.png)

- 短剧管理

  短剧管理功能允许管理员上传、编辑、发布和删除短剧内容，同时管理相关的元数据，确保内容的质量和一致性。

  ![短剧管理](doc/images/管理端/短剧管理.png)

- 分集管理

  针对连续剧或系列短剧，分集管理功能确保每一集的信息准确无误，维护内容的连贯性。

  ![分集管理](doc/images/管理端/分集管理.png)

- 充值管理
  充值管理处理用户的充值请求和支付记录，而金币管理则涉及虚拟货币的发放、使用和交易监控。

  ![充值管理](doc/images/管理端/充值管理.png)

- 金币管理

  充值管理处理用户的充值请求和支付记录，而金币管理则涉及虚拟货币的发放、使用和交易监控。

  ![金币管理](doc/images/管理端/金币管理.png)

- 评论管理

  评论管理功能使管理员能够监控用户评论，维护社区的积极氛围。点赞和收藏功能则帮助分析用户偏好和内容的受欢迎程度。

  ![评论管理](doc/images/管理端/评论管理.png)

- 点赞/收藏
  记录用户互动信息；

  ![点赞/收藏](doc/images/管理端/点赞收藏.png)

- 版本管理

  版本管理确保所有用户都能访问到最新、最稳定的应用程序版本，提升用户体验。

  ![版本管理](doc/images/管理端/版本管理.png)

- 管理员

  管理员功能提供对系统权限和角色的管理；

  ![管理员](doc/images/管理端/管理员.png)

- 用户管理

  用户管理则涉及用户账户的创建、维护和数据管理。

  ![用户管理](doc/images/管理端/用户管理.png)

- 反馈管理

  通过反馈管理，平台可以收集和处理用户的建议和问题报告，不断优化服务和用户体验。

  ![反馈管理](doc/images/管理端/反馈管理.png)

## 移动端

<img src="doc/images/移动端/1.jpg" height="400px" />
  <img src="doc/images/移动端/2.jpg" height="400px" />
  <img src="doc/images/移动端/3.jpg" height="400px" />
  <img src="doc/images/移动端/4.jpg" height="400px" />
  <img src="doc/images/移动端/5.jpg" height="400px" />
  <img src="doc/images/移动端/6.jpg" height="400px" />
  <img src="doc/images/移动端/7.jpg" height="400px" />
  <img src="doc/images/移动端/8.jpg" height="400px" />
  <img src="doc/images/移动端/9.jpg" height="400px" />
  <img src="doc/images/移动端/10.jpg" height="400px" />
  <img src="doc/images/移动端/11.jpg" height="400px" />

## 体验

- APP: http://obs.lemtree.net/mini/application/short_app.apk
- 管理后台：http://admin.yinzuotang.com 用户名/密码：super/1q2w3e4r

# 技术相关

本平台是一个基于 Vue3、Vite3、TypeScript、NaiveUI、Pinia 和 UnoCSS 的清新优雅的中后台模版，它使用了最新流行的前端技术栈，内置丰富的主题配置，有着极高的代码规范，基于文件的路由系统以及基于 Mock 的动态权限路由，开箱即用的中后台前端解决方案，也可用于学习参考。

## 特性

- **最新流行技术栈**：使用 Vue3/Vite 等前端前沿技术开发, 使用高效率的 npm 包管理器 pnpm
- **TypeScript**: 应用程序级 JavaScript 的语言
- **主题**：丰富可配置的主题、暗黑模式，基于原子 css 框架 - UnoCss 的动态主题颜色
- **代码规范**：丰富的规范插件及极高的代码规范
- **文件路由系统**：基于文件的路由系统，根据页面文件自动生成路由声明、路由导入和路由模块
- **权限路由**：提供前端静态和后端动态两种路由模式，基于 mock 的动态路由能快速实现后端动态路由
- **请求函数**：基于 axios 的完善的请求函数封装，提供 Promise 和 hooks 两种请求函数，加入请求结果数据转换的适配器

## 更新日志

[CHANGELOG](./CHANGELOG.md)

## 安装使用

- 环境配置
  **本地环境需要安装 pnpm 7.x 、Node.js 14.18+ 和 Git**
- 克隆代码

```bash
git clone https://gitee.com/king-jump/short-admin.git
```

- 安装依赖

```bash
pnpm i
```

- 运行

```bash
pnpm dev
```

- 打包

```bash
pnpm build
```

- 访问首页

打开本地浏览器访问 `http://localhost`

## Git 贡献提交规范

项目已经内置 Angular 提交规范，直接执行 commit 命令即可生成符合 Angular 提交规范的 commit。

项目已用 simple-git-hooks 代替了 husky, 旧版本用了 husky，执行 pnpm soy init-simple-git-hooks 进行初始化配置

## 浏览器支持

本地开发推荐使用 `Chrome 90+` 浏览器

## 开源作者

[@KingJump](https://github.com/KingJump)

## 特别鸣谢

[@Soybean](https://github.com/soybeanjs)团队

## License

本项目基于[MIT © Soybean-2021](./LICENSE) 协议，仅供参考学习，商用时请保留作者的版权信息，作者不对软件做担保和负责。

# 商务与合作

资深软件研发团队，全员技术达人，提供一站式部署、运维和升级服务。全套系统包含 APP、管理端和视频播放解决方案。

- Email: pehe.smart@gmail.comm
- 微信号：ryu-kt
- 电话：+86 13682136051
