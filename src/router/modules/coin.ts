const coin: AuthRoute.Route = {
  name: 'coin',
  path: '/coin',
  component: 'self',
  meta: { title: '金币管理', icon: 'tabler:coins', singleLayout: 'basic', order: 4 }
};

export default coin;
