const advice: AuthRoute.Route = {
  name: 'advice',
  path: '/advice',
  component: 'self',
  meta: { title: '反馈管理', icon: 'tabler:alert-circle', singleLayout: 'basic', order: 8 }
};

export default advice;
