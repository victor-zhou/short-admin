const version: AuthRoute.Route = {
  name: 'version',
  path: '/version',
  component: 'self',
  meta: { title: '版本管理', icon: 'tabler:versions', singleLayout: 'basic', order: 4 }
};

export default version;
