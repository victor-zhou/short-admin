const favorites: AuthRoute.Route = {
  name: 'favorites',
  path: '/favorites',
  component: 'self',
  meta: { title: '点赞/收藏', icon: 'mdi:star-outline', singleLayout: 'basic', order: 4 }
};

export default favorites;
