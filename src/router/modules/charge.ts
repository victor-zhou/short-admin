const charge: AuthRoute.Route = {
  name: 'charge',
  path: '/charge',
  component: 'self',
  meta: { title: '充值管理', icon: 'mdi:paypal', singleLayout: 'basic', order: 4 },
  children: []
};

export default charge;
