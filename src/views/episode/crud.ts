import type {
  CreateCrudOptionsRet,
  AddReq,
  DelReq,
  EditReq,
  UserPageQuery,
  UserPageRes,
  CreateCrudOptionsProps
} from '@fast-crud/fast-crud';
import { request } from '@/service/request';

function resHandle(res: any) {
  return res.data;
}

export default function createCrudOptions({ context }: CreateCrudOptionsProps): CreateCrudOptionsRet {
  const { apiPrefix, columns, series, router, selectedRowKeys } = context;
  const pageRequest = async (query: UserPageQuery): Promise<UserPageRes> => {
    if (series.value) {
      query.form.series = series.value;
    }
    const res = await request.post(`${apiPrefix}/list`, query);
    return resHandle(res);
  };
  const editRequest = async (ctx: EditReq) => {
    const { form } = ctx;
    const res = await request.put(`${apiPrefix}/update`, form);
    return resHandle(res);
  };
  const delRequest = async (ctx: DelReq) => {
    const { row } = ctx;
    const res = await request.delete(`${apiPrefix}/${row._id}`);
    return resHandle(res);
  };

  const addRequest = async (req: AddReq) => {
    const { form } = req;
    const res = await request.post(`${apiPrefix}`, form);
    return resHandle(res);
  };

  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest
      },
      columns,
      table: {
        rowKey: e => e._id
      },
      rowHandle: {
        fixed: 'right',
        buttons: {
          more: {
            text: null,
            icon: 'mdi:comment-text-multiple-outline',
            size: 'small',
            click: data => {
              console.log('more', data.row._id);
              router.push({
                name: 'comment',
                query: {
                  id: data.row._id,
                  episode: data.row.episode,
                  seriesname: data.row.seriesname
                }
              });
            }
          }
        }
      },
      actionbar: {
        buttons: {
          add: {
            show: true
          },
          isVip: {
            text: '全部付费',
            show: true,
            style: {
              backGrourndColor: '#f56c6c'
            }
            // click: () => {}
          },
          isNotVip: {
            text: '全部免费',
            show: true
          },
          isDown: {
            text: '全部下架',
            show: true
          },
          isUp: {
            text: '全部上架',
            show: true
          }
        }
      },
      settings: {
        plugins: {
          // 行选择插件，内置插件
          rowSelection: {
            // 是否启用本插件
            enabled: true,
            order: -2,
            // 合并在用户配置crudOptions之前还是之后
            before: true,
            props: {
              multiple: true,
              crossPage: true,
              selectedRowKeys,
              onSelectedChanged(selected) {
                console.log('已选择变化：', selected);
              }
            }
            // 如果是内置插件，通过key自动获取handle，否则需要自定义配置
            // handle: (props:any,useCrudProps:UseCrudProps)=>CrudOptions,
            // 你还可以使用 registerCrudOptionsPlugin 方法注册自定义插件，就不需要每次都写配置了
          }
        }
      }
    }
  };
}
