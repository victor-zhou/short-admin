import type {
  AddReq,
  CreateCrudOptionsProps,
  CreateCrudOptionsRet,
  UserPageQuery,
  UserPageRes
} from '@fast-crud/fast-crud';
import { request } from '@/service/request';

function resHandle(res: any) {
  return res.data;
}

export default function createCrudOptions({ context }: CreateCrudOptionsProps): CreateCrudOptionsRet {
  const { apiPrefix, columns, router } = context;
  const pageRequest = async (query: UserPageQuery): Promise<UserPageRes> => {
    const res = await request.post(`${apiPrefix}/list`, query);
    return resHandle(res);
  };

  return {
    crudOptions: {
      request: {
        pageRequest
      },
      columns,
      actionbar: {
        buttons: {
          add: {
            show: false
          }
        }
      },
      rowHandle: {
        fixed: 'right',
        buttons: {
          edit: {
            show: false
          },
          remove: {
            show: false
          }
        }
      }
    }
  };
}
